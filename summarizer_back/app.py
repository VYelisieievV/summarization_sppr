from flask import request, jsonify, Response, Flask
from flask_cors import CORS

from service.summarizer import summary_pipeline

app = Flask(__name__)
CORS(app)


@app.route("/")
def hello_world():
    return "Summary service"


@app.route("/apply", methods=["POST"])
def apply():
    """ Generates and returns summary of the web page """
    sent_num: int = int(request.args.get('sent_num'))
    text: str = request.args.get('text')
    model: str = request.args.get('model')
    cluster: str = request.args.get('cluster')

    site_summary = summary_pipeline(text, model, cluster, sent_num)
    site_summary["Metrics"]["Hopkins"] = str(site_summary["Metrics"]["Hopkins"])
    site_summary["Metrics"]["Silhouette"] = str(site_summary["Metrics"]["Silhouette"])

    return jsonify(site_summary)


@app.route("/send", methods=["POST"])
def send():
    return Response(status=400)