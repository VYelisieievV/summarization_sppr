import numpy as np
import torch
import torch.nn as nn
from sklearn.cluster import KMeans, AgglomerativeClustering
from sklearn.mixture import GaussianMixture
from sklearn.metrics import silhouette_score
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import FeatureUnion
import nltk
from collections import OrderedDict
from transformers import AutoTokenizer, AutoModel
from pyclustertend import hopkins, ivat, vat
from bokeh.plotting import figure
from bokeh.resources import CDN
from bokeh.embed import file_html
from bokeh.layouts import row


def get_vat_plot(matrix, title):
    p = figure(plot_width=350, plot_height=370, title=title, toolbar_location=None)
    p.image(image=[matrix], x=0, y=0, dw=2, dh=2, palette="Greys256")
    p.title.align = "center"
    p.toolbar.active_drag = None
    p.toolbar.active_scroll = None
    p.toolbar.active_tap = None
    p.axis.visible = False
    p.grid.visible = False
    return p


class TfIdfModel(object):
    def __init__(self, mode, random_state=42):
        self.mode = mode
        self.random_state = random_state
        self.clusterizer = None

    def _sep_sentences(self, text):
        """Split text into sentences."""
        return nltk.tokenize.sent_tokenize(text)

    def check_joined_words(self, sent):
        return any([len(word) > 20 for word in sent.split(" ")])

    def filter_sentances(self, sentances):
        sentances = [sent for sent in sentances if not self.check_joined_words(sent)]
        sentances = [sent for sent in sentances if len(sent.split(" ")) > 10]
        return sentances

    def clean_sentances(self, sentances):
        return [sent.lower() for sent in sentances]

    def get_tfidf_sent_vector(self, sentances):
        tfidf_word = TfidfVectorizer(ngram_range=(1, 4), min_df=2, binary=False)
        tfidf_char = TfidfVectorizer(
            ngram_range=(2, 5),
            min_df=5,
            binary=False,
            analyzer="char",
            max_features=500,
        )
        text_vectorizer = FeatureUnion([("word", tfidf_word), ("char", tfidf_char)])

        return text_vectorizer.fit_transform(sentances).todense()

    def _clusterize(self, sent_vectors, n_sentences=5):
        """Get main context sentences for Extraction-based summarization"""
        if self.mode == "gmm":
            model = GaussianMixture(
                n_components=n_sentences, random_state=self.random_state
            ).fit(sent_vectors)
            self.clusterizer = model
            return model.means_
        elif self.mode == "kmeans":
            model = KMeans(n_clusters=n_sentences, random_state=self.random_state).fit(
                sent_vectors
            )
            self.clusterizer = model
            return model.cluster_centers_
        elif self.mode == "agglomirate":
            model = AgglomerativeClustering(n_clusters=n_sentences).fit(sent_vectors)
            self.clusterizer = model
            clusters = [sent_vectors[model.labels_ == i] for i in range(n_sentences)]
            centers = np.array([np.mean(cl, axis=(0)) for cl in clusters])
            return centers
        else:
            raise NotImplementedError('Try mode="gmm" or mode="kmeans"')

    def _get_summary(self, sent_vectors, centroids, sentences):
        selected_sentences = []
        for c in centroids:
            sent_idx = np.argmin([np.linalg.norm(c - vec) for vec in sent_vectors])
            selected_sentences.append(sentences[sent_idx])

        summary = list(OrderedDict.fromkeys(selected_sentences))
        summary.sort(key=lambda i: sentences.index(i))
        return "\n".join(summary)

    def _get_statistics(self, sent_vectors):
        embeds = np.array(sent_vectors)
        hop = hopkins(embeds, embeds.shape[0])
        vati = vat(embeds, return_odm=True)
        ivati = ivat(embeds, return_odm=True)
        labels = self.clusterizer.fit_predict(embeds)
        silhouette = silhouette_score(embeds, labels, metric="euclidean")
        return hop, silhouette, vati, ivati

    def __call__(self, text, n_sentences=5):
        sentences = self._sep_sentences(text)
        sentences = self.filter_sentances(sentences)
        cl_sentences = self.clean_sentances(sentences)
        sent_vectors = self.get_tfidf_sent_vector(cl_sentences)
        centroids = self._clusterize(sent_vectors, n_sentences)
        summary = self._get_summary(sent_vectors, centroids, sentences)
        hopkins_criterion, silhouette, vatimg, ivatimg = self._get_statistics(
            sent_vectors
        )

        p1 = get_vat_plot(vatimg, "Visual assessment of cluster tendency")
        p2 = get_vat_plot(ivatimg, "iVAT")
        p = row(p1, p2)
        html = file_html(p, CDN, "ivat")
        result = {
            "Summary": summary,
            "Metrics": {"Hopkins": hopkins_criterion, "Silhouette": silhouette},
            "Figures": {"html": html},
        }
        return result


class BertSummarizer(object):
    def __init__(self, model, tokenizer, mode="kmeans", random_state=42):
        self.mode = mode
        self.random_state = random_state
        self.bert = model
        self.tokenizer = tokenizer
        self.clusterizer = None

    def _sep_sentences(self, text):
        """Split text into sentences."""

        return nltk.tokenize.sent_tokenize(text)

    def _tokenize(self, sentences):
        """
        Tokenize list of sentences using AutoTokenizer.
        Args:
            sentences(list): list of sentences.
        """
        encoded_input = self.tokenizer(
            sentences, padding=True, truncation=True, max_length=64, return_tensors="pt"
        )
        return encoded_input

    def get_embeddings(self, sentences):
        """
        Get vector represantations for sentences.
        Args:
            sentences(listlike): List of tokenized sentences.
        """
        with torch.no_grad():
            model_output = self.bert(**sentences)
        embeddings = model_output.pooler_output
        embeddings = nn.functional.normalize(embeddings)
        return embeddings

    def _clusterize(self, sent_vectors, n_sentences=5):
        """Get main context sentences for Extraction-based summarization"""
        sent_vectors = sent_vectors.numpy()
        if self.mode == "gmm":
            model = GaussianMixture(
                n_components=n_sentences, random_state=self.random_state
            ).fit(sent_vectors)
            self.clusterizer = model
            return model.means_
        elif self.mode == "kmeans":
            model = KMeans(n_clusters=n_sentences, random_state=self.random_state).fit(
                sent_vectors
            )
            self.clusterizer = model
            return model.cluster_centers_
        elif self.mode == "agglomirate":
            model = AgglomerativeClustering(n_clusters=n_sentences).fit(sent_vectors)
            self.clusterizer = model
            clusters = [sent_vectors[model.labels_ == i] for i in range(n_sentences)]
            centers = np.array([np.mean(cl, axis=(0)) for cl in clusters])
            return centers
        else:
            raise NotImplementedError('Try mode="gmm" or mode="kmeans"')

    def _get_summary(self, sent_vectors, centroids, sentences):
        sent_vectors = sent_vectors.numpy()
        selected_sentences = []
        for c in centroids:
            sent_idx = np.argmin([np.linalg.norm(c - vec) for vec in sent_vectors])
            selected_sentences.append(sentences[sent_idx])

        summary = list(OrderedDict.fromkeys(selected_sentences))
        summary.sort(key=lambda i: sentences.index(i))
        return "\n".join(summary)

    def __call__(self, text, n_sentences=5):
        sentences = self._sep_sentences(text)
        tokenized_sentences = self._tokenize(sentences)
        embeds = self.get_embeddings(tokenized_sentences)
        context_points = self._clusterize(embeds, n_sentences)
        summary = self._get_summary(embeds, context_points, sentences)
        hopkins_criterion, silhouette, vatimg, ivatimg = self._get_statistics(embeds)

        p1 = get_vat_plot(vatimg, "Visual assessment of cluster tendency")
        p2 = get_vat_plot(ivatimg, "iVAT")
        p = row(p1, p2)
        html = file_html(p, CDN, "ivat")
        result = {
            "Summary": summary,
            "Metrics": {"Hopkins": hopkins_criterion, "Silhouette": silhouette},
            "Figures": {"html": html},
        }
        return result

    def _get_statistics(self, sent_vectors):
        embeds = sent_vectors.numpy()
        hop = hopkins(embeds, embeds.shape[0])
        vati = vat(embeds, return_odm=True)
        ivati = ivat(embeds, return_odm=True)
        labels = self.clusterizer.fit_predict(embeds)
        silhouette = silhouette_score(embeds, labels, metric="euclidean")
        return hop, silhouette, vati, ivati


def summary_pipeline(text, model, cluster_alg, n_sent):
    try:
        nltk.download("punkt")
    except Exeption as e:
        print(e)
        exit()
    if model == "tfidf":
        summary_writter = TfIdfModel(mode=cluster_alg)
    elif model == "labse":
        tokenizer = AutoTokenizer.from_pretrained("./service/LaBSE")
        model = AutoModel.from_pretrained("./service/LaBSE")
        summary_writter = BertSummarizer(model, tokenizer, mode=cluster_alg)
    else:
        raise NotImplementedError("Not possible model")

    output = summary_writter(text, n_sentences=n_sent)
    return output
