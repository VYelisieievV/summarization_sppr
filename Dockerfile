FROM python:3.6-jessie

# Copy the base uWSGI ini file to enable default dynamic uwsgi process number
COPY /summarizer_back /summarizer_back

# Add app
WORKDIR /summarizer_back

# Make /app/* available to be imported by Python globally
ENV PYTHONPATH=/summarizer_back

RUN pip install -r requirements.txt

EXPOSE 5000

CMD ["python", "run.py"]